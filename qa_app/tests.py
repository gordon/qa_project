from django.test import TestCase, Client
from django.contrib.auth import get_user_model
from django.urls import reverse
from pyquery import PyQuery as pq

from . import models


User = get_user_model()


class QaTestCase(TestCase):
    fixtures = ["demo_data.yaml"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.client = Client()

    def test_question_get_answered_questions(self):
        models.Question.objects.all().delete()
        questions = models.Question.objects.get_answered_questions()
        self.assertEqual(questions.count(), 0, msg="La base devrait être vide")

    def test_question_get_answered_questions_with_some_objects(self):
        questions = models.Question.objects.get_answered_questions()
        self.assertEqual(
            questions.count(),
            50,
            msg="La méthode n’a pas récupéré le bon nombre de questions",
        )
        for q in questions:
            self.assertNotEqual(q.answer.id, None)

    def test_question_get_answered_questions_with_no_answer(self):
        models.Answer.objects.all().delete()
        questions = models.Question.objects.get_answered_questions()
        self.assertEqual(questions.count(), 0, msg="La base devrait être vide")

    def test_index_page(self):
        res = self.client.get("/")
        self.assertEqual(res.status_code, 200)
        self.assertEqual(
            res.resolver_match.url_name,
            "index",
            msg="La page d’accueil n’est pas nommée index",
        )

    def test_homepage_simple(self):
        res = self.client.get(reverse("index"))
        self.assertEqual(res.status_code, 200)
        self.assertIn(
            "question_list",
            res.context,
            msg="La vue n’a pas renvoyé de liste de questions",
        )
        self.assertEqual(
            res.context["question_list"].count(),
            10,
            msg="On ne devrait avoir que 10 questions dans le contexte, à cause de la pagination",
        )
        self.assertEqual(
            res.context["paginator"].count,
            50,
            msg="Le paginateur devrait contenir 50 objets",
        )
        req = pq(res.content)
        form = req("form[method='post']")
        self.assertEqual(len(form), 1, msg="Le formulaire n’apparaît pas dans la page")
        self.assertEqual(form.attr("enctype"), "multipart/form-data")

    def test_homepage_try_to_create_question(self):
        req = self.client.post(
            reverse("index"), data={"title": "foo", "text": "bar"}, follow=False
        )
        self.assertEqual(req.status_code, 302)
        self.assertEqual(
            req.get("location"),
            reverse("index"),
            msg="La redirection du formulaire est incorrecte",
        )
        qs = models.Question.objects.filter(title="foo", text="bar")
        self.assertEqual(
            qs.count(), 1, msg="Le formulaire valide n’a pas créé de question"
        )
        q = qs.first()
        with self.assertRaises(models.Question.answer.RelatedObjectDoesNotExist):
            q.answer
