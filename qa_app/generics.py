from django.views import generic
from django.views.generic import edit
from django.contrib import messages


class ListAndCreateView(generic.ListView, edit.ModelFormMixin, edit.ProcessFormView):

    object = None
    success_message = "L'objet {obj} a été créé avec succès."

    def form_valid(self, form):
        messages.success(self.request, self.success_message.format(obj=self))
        return super().form_valid(form)

    def form_invalid(self, form):
        self.object_list = self.get_queryset()
        return super().form_invalid(form)


class SearchMixin(generic.ListView):

    search_query = ""

    def get_queryset(self):
        qs = super().get_queryset()
        self.search_query = self.request.GET.get("search", "")
        if self.search_query:
            qs = self.model.objects.search(self.search_query, qs=qs)
        return qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["search"] = self.search_query
        return context
